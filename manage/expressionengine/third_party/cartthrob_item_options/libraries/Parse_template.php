<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (class_exists('Parse_template'))return; 
/**
 * @property CI_Controller $EE
 * @property Template $TMPL
 */
class Parse_template
{
	var $package_path = NULL;
	
	public function __construct($params = array())
	{
		$this->EE =& get_instance();
		
		if ( ! isset($this->EE->TMPL))
		{
			$this->EE->load->library('template', NULL, 'TMPL');
		}
		if (!empty($params['package_path']))
		{
			$this->package_path = PATH_THIRD.$params['package_path']; 
		}
	}
	public function parse($template, $variables = array(), $constants = array(), $run_template_engine = FALSE)
	{
		foreach ($constants as $key => $value)
		{
			$template = str_replace($key, $value, $template);
		}
		
		if ($variables)
		{
			$template = $this->EE->TMPL->parse_variables($template, array($variables));
		}
		
		if ($run_template_engine)
		{
			$this->EE->TMPL->parse($template);
			
			//restore package path
			if (!empty($this->pacakge_path))
			{
				$this->EE->load->add_package_path($this->package_path);
			}
			
			$template = $this->EE->TMPL->final_template;
		}
		
		return $template;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php echo $form_edit; ?>

  	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th>
					<strong><?=lang($module_name."_label_view")?></strong> 
 				</th>
				<th>
					<strong><?=lang($module_name."_short_name_view")?></strong> 
 				</th>
				<th colspan="3">
					<strong><?=lang($module_name."_actions")?></strong> 
 				</th>

			</tr>
		</thead>
		<tbody>
			<?php foreach ($view as $key => $value):  
				$count = (!isset($count) ? 1 : $count+1);
				 
			 ?>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<?=$value['label']?>
				</td>
				<td>
					<?=$value['short_name']?>
				</td>
				<td>
					 <a href="<?php echo $edit_href. $value['id'];  ?>"> <?=lang('edit')?> &raquo;</a>
					 <a href="<?php echo $delete_href. $value['id'];  ?>"> <?=lang('delete')?> &raquo;</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
</form>
	
<?php echo $pagination; ?>
<?php

function chart_data($data, $days = 30, $date_format = 'M jS')
{
	$stats = array();
	$i = 0;
	
	if($days < count($data))
	{
		$data = array_slice($data, -$days, $days);
	}
	
	foreach($data as $date => $row)
	{
		$stats[$i] = "['".date($date_format, strtotime($date.' 12:00:00'))."', ";
		$stats[$i] .= $row['visits'].', ';
		$stats[$i] .= $row['pageviews'].']';
		$i++;
	}
	return implode(',', $stats);
}


function link_url($url, $title)
{
	return '<a href="'.QUERY_MARKER.'URL='.urlencode($url).'" target="_blank">'.$title.'</a>';
}


function sparkline($url, $size = null, $width = '120', $height='20')
{
	if(strpos($url, 'chart.googleapis.com') && !empty($size))
	{
		$url = str_replace('120x20', $size, $url);
	}
	return '<img src="'.$url.'" width="'.$width.'" height="'.$height.'" alt="" />';
}
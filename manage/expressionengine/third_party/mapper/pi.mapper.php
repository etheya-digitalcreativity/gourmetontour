<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mapper Plugin
 *
 * @package		ExpressionEngine 2
 * @subpackage	Third Party
 * @category	Modules
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code
 */

$plugin_info = array(
	'pi_name' => 'Mapper',
	'pi_version' => '1.0.0',
	'pi_author' => 'Phil Sturgeon',
	'pi_author_url' => 'http://philsturgeon.co.uk/',
	'pi_description' => 'Display Google Maps with simple tags.',
	'pi_usage' => Mapper::usage()
);

class Mapper
{
	public $return_data = '';

	function Mapper()
	{
		$this->EE =& get_instance();
	
		$params['width'] 		= $this->EE->TMPL->fetch_param('width', '100%');
		$params['height'] 		= $this->EE->TMPL->fetch_param('height', '300px');
		$params['address'] 		= $this->EE->TMPL->fetch_param('address', '');
		$params['zoom'] 		= $this->EE->TMPL->fetch_param('zoom', 15);
		$params['description'] 	= $this->EE->TMPL->fetch_param('description', '');

		$orig_view_path = $this->EE->load->_ci_view_path;
		$this->EE->load->_ci_view_path = APPPATH.'third_party/mapper/views/';
		$return = $this->EE->load->view('map', array(
			'params' => $params,
			'canvas_id' => $canvas_id = mt_rand()
		), TRUE);
		$this->EE->load->_ci_view_path = $orig_view_path;
		
		// Fire off this map specific JS
		return $this->return_data = '<script type="text/javascript">'.$return.'</script>'
			 . '<div id="gmap_'.$canvas_id.'"></div>';
	}
	// END
// ----------------------------------------
//  Plugin Usage
// ----------------------------------------
// This function describes how the plugin is used.
//  Make sure and use output buffering

	function usage()
	{
		ob_start();
?>
		Mapper usage stuffs

<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}
}

/* End of file pi.mapper.php */
/* Location: system/expressionengine/third_party/mapper/pi.mapper.php */

<div id="shortlist_container" class="mor">


	<?php if($show_favorites_option) : ?>
		<div class="tg" style="">
			<h2>Import Existing Data from <em>Solspace's Favorites</em></h2>
			<div class="alert">
				<a href="<?=$import_favs_uri?>" class="btn add">Configure &amp; Import</a>
				Your site appears to be currently using Solspace's Favorites. We can import this existing data over to Shortlist.
			</div>
		</div>
	<?php endif; ?>

	<?php if($show_vc_todo_option) : ?>
		<div class="tg" style="">
			<h2>Import Existing Data from <em>VC Todo Addon</em></h2>
			<div class="alert">
				<a href="<?=$import_custom_uri?>" class="btn add">Configure &amp; Import</a>
				There appears to be data in a format we recognise in the <em>exp_vc_todo</em> table. We can import this data over to Shortlist.
			</div>
		</div>
	<?php endif; ?>	



<?php if(!$show_vc_todo_option AND !$show_favorites_option) : ?>
	<div class="tg" style="">
		<h2>No Imports Available</h2>
		<div class="alert">
			Shortlist can import data from Solspace's Favorites, and some other custom addons. <br/>
			It doesn't look like you have available data to import right now. If you have data you need importing to Shortlist, contact <a href="mailto:support@squarebit.co.uk?subject=Shortlist Importer Request">SquareBit Support</a> and we'll try and help you out.

		</div>
	</div>
<?php endif; ?>

</div>
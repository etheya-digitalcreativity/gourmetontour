<div id="shortlist_container" class="mor">

	<div class="tg" style="width:49%; margin-right :1%; float:left;">
		<h2><?=lang_switch('shortlist_list_item', $item_count)?> : <?=$item_count?> <?=lang_switch('shortlist_item', $item_count )?> - <span class="small" style="font-size : 0.8em"><?=$internal_count?>  <?=lang('shortlist_internal')?> / <?=$external_count?> <?=lang('shortlist_external')?></span></h2>

		<table class="data">
			<thead>
				<tr style="background-color :transparent">
					<th><?=lang('shortlist_title')?></th>
					<th><?=lang('count')?></th>
					<th><?=lang('type')?></th>
				</tr>
			</head>
			<tbody>

				<?php foreach( $items as $item ) : ?>
				<tr>
					<td><a href="<?=$item['item_detail_url']?>"><?=$item['title']?></a></td>
					<td><?=$item['c']?></td>
					<td><?=lang('shortlist_'.$item['type'])?></td>
				</tr>
				<?php endforeach; ?>

			</tbody>

		</table>
	</div>

	<div class="tg" style="width:49%;float:left;">
		<h2><?=lang_switch('shortlist_user_list', $list_count)?> : <?=$list_count?> <?=lang_switch('shortlist_list', $list_count)?> </h2>

		<table class="data">
			<thead>
				<tr style="background-color :transparent">
					<th><?=lang('shortlist_user_or_guest')?></th>
					<th><?=lang('item_count')?></th>
					<th><?=lang('shortlist_last_active')?></th>
				</tr>
			</head>
			<tbody>

				<?php foreach( $lists as $list ) : ?>
				<tr>
					<td><a href="<?=$list['list_detail_url']?>">
						<?php if( $list['user_type'] == 'guest' ) : ?>[ <?=lang('shortlist_guest')?> ]
						<?php else : ?><?=$members[ $list['member_id'] ]['screen_name']?><?php endif;?></a></td>
					<td><?=$list['c']?></td>
					<td><?=$list['last_activity_since']?></td>
				</tr>
				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

</div>